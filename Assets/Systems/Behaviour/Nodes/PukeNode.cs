﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class PukeNode : ActionNode
{
    protected override void OnStart(BehaviourTreeRunner runner)
    {
        if (runner.TryGetComponent<Fish>(out var fish))
        {
            fish.Puke();
        }
        state = EState.Success;
    }

    protected override void OnStop(BehaviourTreeRunner runner)
    {
        
    }

    protected override EState OnUpdate(BehaviourTreeRunner runner)
    {
        return state;
    }
}
