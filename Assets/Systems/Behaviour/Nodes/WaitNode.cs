using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitNode : ActionNode
{
    public float duration = 1;
    float startTime;

    protected override void OnStart(BehaviourTreeRunner runner)
    {
        startTime = Time.time;
    }

    protected override void OnStop(BehaviourTreeRunner runner)
    {
        
    }

    protected override EState OnUpdate(BehaviourTreeRunner runner)
    {
        if (Time.time - startTime > duration)
        {
            return EState.Success;
        }
        return EState.Running;
    }
}
