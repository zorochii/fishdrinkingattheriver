using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] Animator anim;
    [SerializeField] BodyVisuals visuals;
    [Header("Start State")]
    [SerializeField] bool sitting;

    int hash_BSit;

    private void Awake()
    {
        hash_BSit = Animator.StringToHash("Sit");

        if (sitting)
        {
            anim.SetBool(hash_BSit, true);
            anim.Play("bodySit");
        }
    }

    public void Interact()
    {
        if (TryGetComponent<BehaviourTreeRunner>(out var runner))
        {
            runner.Trigger(BehaviourTreeRunner.ETrigger.Interact);
        }
    }

    public void Puke()
    {
        visuals.Puke();
    }
}
