﻿
using System;
using UnityEngine;
using UnityEditor.Experimental.GraphView;
using UnityEditor;
using UnityEngine.UIElements;
using UnityEditor.UIElements;

public class NodeView : UnityEditor.Experimental.GraphView.Node
{
    public Action<NodeView> OnNodeSelected;

    public Node node;
    public Port input;
    public Port output;

    public NodeView(Node node) : base("Assets/Systems/Behaviour/Editor/NodeView.uxml")
    {
        this.node = node;
        var n = node.name;
        if (n.Substring(n.Length - 7) == "(Clone)")
        {
            n = n.Substring(0, n.Length - 7);
        }
        else
        {
            AddToClassList("original");
        }
        if (n.Substring(n.Length - 4) == "Node") n = n.Substring(0, n.Length - 4);
        this.title = ObjectNames.NicifyVariableName(n);
        this.viewDataKey = node.guid;
        style.left = node.position.x;
        style.top = node.position.y;

        CreateInputPorts();
        CreateOutputPorts();
        SetupClasses();

        Label descriptionLabel = this.Q<Label>("description");
        descriptionLabel.bindingPath = "description";
        descriptionLabel.Bind(new SerializedObject(node));
    }

    private void SetupClasses()
    {
        if (node is ActionNode)
        {
            AddToClassList("actionNode");
        }
        else if (node is CompositeNode)
        {
            AddToClassList("compositeNode");
        }
        else if (node is DecoratorNode)
        {
            AddToClassList("decoratorNode");
        }
        else if (node is RootNode)
        {
            AddToClassList("rootNode");
        }
    }

    private void CreateInputPorts()
    {
        if (node is ActionNode)
        {
            input = InstantiatePort(Orientation.Horizontal, Direction.Input, Port.Capacity.Single, typeof(bool));
        } else if (node is CompositeNode)
        {
            input = InstantiatePort(Orientation.Horizontal, Direction.Input, Port.Capacity.Single, typeof(bool));
        } else if (node is DecoratorNode)
        {
            input = InstantiatePort(Orientation.Horizontal, Direction.Input, Port.Capacity.Single, typeof(bool));
        } else if (node is RootNode)
        {

        }

        if (input != null)
        {
            input.portName = "";
            inputContainer.Add(input);
        }
    }

    private void CreateOutputPorts()
    {
        if (node is ActionNode)
        {
            
        }
        else if (node is CompositeNode)
        {
            output = InstantiatePort(Orientation.Horizontal, Direction.Output, Port.Capacity.Multi, typeof(bool));
        }
        else if (node is DecoratorNode)
        {
            output = InstantiatePort(Orientation.Horizontal, Direction.Output, Port.Capacity.Single, typeof(bool));
        }
        else if (node is RootNode)
        {
            output = InstantiatePort(Orientation.Horizontal, Direction.Output, Port.Capacity.Single, typeof(bool));
        }

        if (output != null)
        {
            output.portName = "";
            outputContainer.Add(output);
        }
    }

    public override void SetPosition(Rect newPos)
    {
        base.SetPosition(newPos);
        Undo.RecordObject(node, "Behaviour Tree (Set Position)");
        node.position.x = newPos.xMin;
        node.position.y = newPos.yMin;
        EditorUtility.SetDirty(node);
    }

    public override void OnSelected()
    {
        base.OnSelected();
        OnNodeSelected?.Invoke(this);
    }

    public void SortChildren()
    {
        CompositeNode composite = node as CompositeNode;
        if (composite)
        {
            composite.children.Sort(SortByPosition);
        }
    }

    private int SortByPosition(Node top, Node bottom)
    {
        return top.position.y < bottom.position.y ? -1 : 1;
    }

    public void UpdateState()
    {
        RemoveFromClassList("runningState");
        RemoveFromClassList("failureState");
        RemoveFromClassList("successState");

        if (!Application.isPlaying) return;

        switch (node.state)
        {
            case Node.EState.Running:
                if (node.started)
                {
                    AddToClassList("runningState");
                }
                break;
            case Node.EState.Failure:
                AddToClassList("failureState");
                break;
            case Node.EState.Success:
                AddToClassList("successState");
                break;
        }
    }
}