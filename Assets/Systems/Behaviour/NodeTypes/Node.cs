using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Node : ScriptableObject
{
    public enum EState
    {
        Running, Failure, Success
    }

    [HideInInspector] public EState state = EState.Running;
    [HideInInspector] public bool started = false;
    [HideInInspector] public string guid;
    [HideInInspector] public Vector2 position;
    [HideInInspector] public Blackboard blackboard;
    [TextArea] public string description;
    
    public EState Execute(BehaviourTreeRunner runner)
    {
        if (!started)
        {
            OnStart(runner);
            started = true;
        }
        state = OnUpdate(runner);
        if (state != EState.Running)
        {
            OnStop(runner);
            started = false;
        }
        return state;
    }

    internal void Reset()
    {
        state = EState.Running;
        started = false;
        OnReset();
    }

    public virtual void OnReset() { }

    public virtual Node Clone()
    {
        return Instantiate(this);
    }

    protected abstract void OnStart(BehaviourTreeRunner runner);
    protected abstract void OnStop(BehaviourTreeRunner runner);
    protected abstract EState OnUpdate(BehaviourTreeRunner runner);
}
