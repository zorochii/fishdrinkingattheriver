using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundNode : ActionNode
{
    public AudioClip clip;
    public float volume = 1;

    protected override void OnStart(BehaviourTreeRunner runner)
    {
        AudioSource.PlayClipAtPoint(clip, runner.transform.position, volume);
    }

    protected override void OnStop(BehaviourTreeRunner runner)
    {
        
    }

    protected override EState OnUpdate(BehaviourTreeRunner runner)
    {
        return EState.Success;
    }
}
