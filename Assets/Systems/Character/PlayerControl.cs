using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public static GameControls gameControls;

    [Header("Components")]
    [SerializeField] Transform viewTransform;
    [SerializeField] Rigidbody rbody;
    [Header("Settings")]
    [SerializeField] float moveSpeed = 5f;
    [SerializeField] float viewSpeed = 180f;
    [SerializeField] Vector2 viewClamp = new Vector2(-45, 45);

    float viewH;
    float viewV;

    private void Awake()
    {
        gameControls = new GameControls();
        gameControls.Enable();
        gameControls.Player.Enable();
    }

    private void Start()
    {
        viewH = rbody.rotation.eulerAngles.y;

        gameControls.Player.Start.performed += Interact;
    }

    private void Interact(UnityEngine.InputSystem.InputAction.CallbackContext ctx)
    {
        if (TitleScreen.PlayState)
        {
            Fish currF = null;
            float currDst = Mathf.Infinity;
            foreach (var f in activeInteractions)
            {
                float dst = Vector3.Distance(transform.position, f.transform.position);
                if (dst < currDst)
                {
                    currF = f;
                    currDst = dst;
                }
            }
            // Execute fish (puke for now).
            if (currF != null) currF.Interact();
        }
    }

    private void Update()
    {
        if (!TitleScreen.PlayState) return;

        var move = gameControls.Player.Move.ReadValue<Vector2>();
        var view = gameControls.Player.View.ReadValue<Vector2>();

        var movement = Quaternion.Euler(0, viewTransform.rotation.eulerAngles.y, 0) * new Vector3(move.x, 0, move.y) * Time.deltaTime * moveSpeed;
        rbody.position += movement;

        viewH += view.x * Time.deltaTime * viewSpeed;
        viewV = Mathf.Clamp(viewV - (view.y * Time.deltaTime * viewSpeed), viewClamp.x, viewClamp.y);
        rbody.rotation = Quaternion.Euler(viewV, viewH, 0);
    }

    List<Fish> activeInteractions = new List<Fish>();

    private void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody != null && other.attachedRigidbody.TryGetComponent<Fish>(out var fish))
        {
            if (!activeInteractions.Contains(fish)) activeInteractions.Add(fish);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.attachedRigidbody != null && other.attachedRigidbody.TryGetComponent<Fish>(out var fish))
        {
            if (activeInteractions.Contains(fish)) activeInteractions.Remove(fish);
        }
    }
}
