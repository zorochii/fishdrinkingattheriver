using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatNode : DecoratorNode
{
    protected override void OnStart(BehaviourTreeRunner runner)
    {
        
    }

    protected override void OnStop(BehaviourTreeRunner runner)
    {
        
    }

    protected override EState OnUpdate(BehaviourTreeRunner runner)
    {
        child.Execute(runner);
        return EState.Running;
    }
}
