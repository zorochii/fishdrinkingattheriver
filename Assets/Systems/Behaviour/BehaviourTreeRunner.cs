using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourTreeRunner : MonoBehaviour
{
    public enum ETrigger
    {
        Awake, Collide, Interact
    }
    public enum EOnEnd
    {
        Once, Reset, Repeat
    }

    [SerializeField] BehaviourTree tree;
    [SerializeField] ETrigger trigger;
    [SerializeField] EOnEnd OnEnd;

    public BehaviourTree Tree => tree;
    bool running;

    void Start()
    {
        tree = tree.Clone();
        tree.Bind(this);

        if (trigger == ETrigger.Awake) running = true;
    }

    void Update()
    {
        if (running)
        {
            tree.Update();
            if (tree.treeState != Node.EState.Running)
            {
                if (OnEnd != EOnEnd.Once) tree.Reset();
                if (OnEnd != EOnEnd.Repeat) running = false;
            }
        }
    }

    public void Trigger(ETrigger kind)
    {
        if (kind == trigger) running = true;
    }
}
