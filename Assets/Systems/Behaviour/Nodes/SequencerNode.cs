using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SequencerNode : CompositeNode
{
    int currIdx;

    protected override void OnStart(BehaviourTreeRunner runner)
    {
        currIdx = 0;
    }

    protected override void OnStop(BehaviourTreeRunner runner)
    {
        
    }

    protected override EState OnUpdate(BehaviourTreeRunner runner)
    {
        var child = children[currIdx];
        switch (child.Execute(runner))
        {
            case EState.Running:
                return EState.Running;
            case EState.Failure:
                return EState.Failure;
            case EState.Success:
                currIdx++;
                break;
        }
        return currIdx >= children.Count ? EState.Success : EState.Running;
    }

    public override void OnReset()
    {
        currIdx = 0;
    }
}