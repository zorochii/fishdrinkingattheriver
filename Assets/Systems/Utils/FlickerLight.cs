using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickerLight : MonoBehaviour
{
    [SerializeField] float flickerLength;
    [SerializeField] float flickerSpeed;
    Light light;
    private float origIntensity;

    private void Awake()
    {
        light = GetComponent<Light>();
        origIntensity = light.intensity;
    }

    void Update()
    {
        light.intensity = origIntensity + Mathf.PingPong(Time.time * flickerSpeed, flickerLength);
    }
}
