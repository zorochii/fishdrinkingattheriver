using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugLogNode : ActionNode
{
    public string message;

    protected override void OnStart(BehaviourTreeRunner runner)
    {
        Debug.Log(message);
    }

    protected override void OnStop(BehaviourTreeRunner runner)
    {
        
    }

    protected override EState OnUpdate(BehaviourTreeRunner runner)
    {
        return EState.Success;
    }
}
