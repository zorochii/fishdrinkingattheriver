using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TitleScreen : MonoBehaviour
{
    [SerializeField][Range(0, 1)] float titleToAction;
    [SerializeField] Transform titleTransform;
    [SerializeField] Transform yourVisionTransform;
    [SerializeField] CanvasGroup titleCanvas;

    public static bool PlayState;
    Camera mainCamera;

    void Start()
    {
        mainCamera = Camera.main;
        titleToAction = 0;
        PlayerControl.gameControls.Player.Start.performed += PressedStart;
    }

    private void PressedStart(InputAction.CallbackContext ctx)
    {
        if (!PlayState)
        {
            StartCoroutine(GoTo(true));
        }
    }

    IEnumerator GoTo(bool state)
    {
        PlayState = state;
        titleCanvas.blocksRaycasts = false;
        float perc = 0;
        while (perc < 1)
        {
            perc += Time.unscaledDeltaTime;
            titleToAction = (state ? perc : 1 - perc);
            titleCanvas.alpha = 1 - titleToAction;
            yield return null;
        }
        titleToAction = (state ? 1 : 0);
        titleCanvas.blocksRaycasts = !state;
    }

    void LateUpdate()
    {
        var currPos = Vector3.Lerp(titleTransform.position, yourVisionTransform.position, titleToAction);
        var currRot = Quaternion.Lerp(titleTransform.rotation, yourVisionTransform.rotation, titleToAction);
        mainCamera.transform.position = currPos;
        mainCamera.transform.rotation = currRot;
    }
}
